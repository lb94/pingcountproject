FROM node:alpine
ADD server.js /server.js
ADD package.json /package.json
CMD npm install && npm start
